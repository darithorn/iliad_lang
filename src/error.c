#include "error.h"
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#define error_buffer_append_char(c) { error_buffer[error_buffer_pos] = c; error_buffer_pos++; }

static size_t error_buffer_size = 128;
static size_t error_buffer_pos = 0;
static char *error_buffer;

void
error_buffer_append(char *msg) {
    // append a newline onto the buffer
    error_buffer_append_char('\n');
    int c;
    int i = 0;
    do {
        c = msg[i];
        if(c == '\0') return;
        error_buffer_append_char(c);
        i++;
        if(error_buffer_pos + 1 >= error_buffer_size) {
            error_buffer_size *= 1.5f;
            error_buffer = realloc(error_buffer, error_buffer_size);
        }
    } while(error_buffer_pos < error_buffer_size);
}

error_t
new_error(char *msg, ...) {
    error_t err = {};
    va_list args;
    va_start(args, msg);
    sprintf(err.msg, msg, args);
    error_buffer_append(err.msg);
    va_end(args);
    return err;
}

void
output_error_buffer() {
    printf("%s\n", error_buffer);
}