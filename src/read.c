#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include "read.h"
#include "error.h"

buffer_t
new_buffer(const char* filename, error_t *err) {
    buffer_t buffer = {};
    buffer.pos = -1;
    FILE *tmp = fopen(filename, "r");
    if(tmp == NULL) { 
        *err = new_error("cannot open file: %s", filename);
        return buffer;
    }
    fseek(tmp, 0, SEEK_END);
    buffer.size = ftell(tmp);
    fseek(tmp, 0, SEEK_SET);
    buffer.data = (char *)malloc(sizeof(char) * buffer.size);
    if(buffer.data == NULL) {
        *err = new_error("unable to allocate memory for buffer data");
        return buffer;
    }
    fread(buffer.data, sizeof(char), buffer.size, tmp);
    fclose(tmp);
    return buffer;
}

void
clean_buffer(buffer_t *buffer) {
    free(buffer->data);
}

/// Sets the current buffer position by +1 and retrieves the current character
int
get_next(buffer_t *buffer) {
    if(buffer->pos + 1 >= buffer->size) return EOF;
    buffer->pos++;
    return buffer->data[buffer->pos];
}

/// Sets the current buffer position by -1 and retrieves the current character
int
get_prev(buffer_t *buffer) {
    if(buffer->pos == 0) return EOF;
    buffer->pos--;
    return buffer->data[buffer->pos];
}

int
peek_char(buffer_t *buffer) {
    if(buffer->pos + 1 >= buffer->size) return EOF;
    return buffer->data[buffer->pos + 1];
}

int
is_num(char c) {
    return c >= 48 && c <= 57;
}

int
is_letter(char c) {
    return (c >= 65 && c <= 90) || (c >= 97 && c <= 122);
}

int
is_special_char(char c) {
    return c >= 33 && c <= 47;
}

int
is_accepted_char(char c) {
    return is_letter(c) || is_num(c) ||
           c == '!' || c == '$' || c == '%' || c == '&' || c == '*' || c == '+' || c == '-' || c == '.' || c == '/' ||
           c == ':' || c == '<' || c == '=' || c == '>' || c == '?' || c == '@' || c == '^' || c == '_' || c == '~';
}

int
is_whitespace(char c) {
    int i = 0;
    for(; WHITESPACE_DELIMITERS[i] != '\0'; i++) {
        if(c == WHITESPACE_DELIMITERS[i]) {
            return 1;
        }
    }
    return 0;
}

int
get_next_word(buffer_t *buffer, char word[256], char *delimiters, int eat, error_t *err) {
    memset(word, '\0', 256);
    word[0] = TOKEN_TYPE_WORD;
    int c;
    int i = 1;
    do {
        c = get_next(buffer);
        // Check each specified delimiter in delimiters
        int x;
        for (x = 0; delimiters[x] != '\0'; x++) {
            if (delimiters[x] == c) {
                if (!eat) {
                    // eat the delimiter or not
                    get_prev(buffer);
                }
                RETURN_NO_ERR(); // END
            }
        }
        if(!is_accepted_char(c)) {
            if(c != EOF) get_prev(buffer);
            RETURN_NO_ERR();
        }
        word[i] = c;
        i++;
    } while(c != EOF);
    THROW_ERR("Unexpected end of file.");
}

int 
read_number(buffer_t *buffer, char number[256], error_t *err) {
    memset(number, '\0', 256);
    number[0] = TOKEN_TYPE_NUMBER;
    int i = 1; // current index for 'number'
    int decimal = 0; // whether number has decimal
    int expect_next = 0; // expect number next (used for decimal)
    int c;
    do {
        int append = 0;     
        c = peek_char(buffer);
        if (c == '.') {
            if (decimal) {
                THROW_ERR("Multiple decimals!");
            }
            decimal = 1;
            expect_next = 1;
            append = 1;
        } else if (isdigit(c)) {
            expect_next = 0;
            append = 1;
        } else {
            if (expect_next) {
                THROW_ERR("Expected digit!");
            }
            RETURN_NO_ERR();
        }
        // Avoids duplication of code
        if (append) {
            int tmp = get_next(buffer);
            if(tmp == EOF) THROW_ERR("unexpected end of file");
            number[i] = c;
            i++;
        }
    } while(c != EOF);
    THROW_ERR("Unexpected end of file.");
}

int 
read_string(buffer_t *buffer, char str[256], error_t *err) {
    int c = get_next(buffer); // assumes we already know first character was double quote
    if(c != '"') THROW_ERR("expected starting double quotation mark for string.");
    memset(str, '\0', 256);
    str[0] = TOKEN_TYPE_STRING;
    int i = 1;
    int escaped = 0;
    do {
        c = get_next(buffer);
        if (c == '\"' && !escaped) {
            // if c is double quote
            // and not escaped then must be end of string
            RETURN_NO_ERR();
        } else {
            if (!escaped && c == '\\') {
                // if currently not escaped and \
                // then must be escape character
                escaped = 1;
            } else {
                if (escaped) {
                    // all the escape characters
                    switch (c) {
                        case 'a':
                            str[i] = '\a';
                            break;
                        case 'b':
                            str[i] = '\b';
                            break;
                        case 'f':
                            str[i] = '\f';
                            break;
                        case 'n':
                            str[i] = '\n';
                            break;
                        case 'r':
                            str[i] = '\r';
                            break;
                        case 't':
                            str[i] = '\t';
                            break;
                        case 'v':
                            str[i] = '\v';
                            break;
                        case '\\':
                            str[i] = '\\';
                            break;
                        case '\'':
                            str[i] = '\'';
                            break;
                        case '"':
                            str[i] = '"';
                            break;
                        case '?':
                            str[i] = '\?';
                            break;
                        default:
                            THROW_ERR("Unknown escape sequence!");
                    }
                    escaped = 0;
                } else {
                    str[i] = c;
                }
                i++;
            }
        }
    } while(c != EOF);
    THROW_ERR("Unexpected end of file.");
}

int
read_hash(buffer_t *buffer, char result[256], error_t *err) {
    int c = get_next(buffer);
    if(c == EOF) {
        THROW_ERR("unexpected end of file");
    }
    result[1] = c;
    c = get_next(buffer);
    if(c == EOF) {
        THROW_ERR("unexpected end of file");
    }
    if(c == 't' || c == 'f') {
        result[0] = TOKEN_TYPE_BOOL;
        result[2] = c;
    } else if(c == '\\') {
        result[0] = TOKEN_TYPE_CHAR;
        result[2] = '\\';
        result[3] = get_next(buffer);
        if(result[3] == '\\') {
            // escaped character
            result[4] = get_next(buffer);
        }
    } else if(c == '(') {
        result[0] = TOKEN_TYPE_VECTOR;
        result[2] = '(';
        int i = 3;
        int count = 1;
        for(; count != 0; c = get_next(buffer)) {
            if(c == '(') {
                count++;
            } else if(c == ')') {
                count--;
            }
            result[i] = c;
            i++;
            if(i >= 256) {
                THROW_ERR("too many characters for vector.");
            }
            if(c == EOF) THROW_ERR("unexpected EOF");
        }
    } else {
        THROW_ERR("unexpected character after #");
    }
    RETURN_NO_ERR();
}

int
read_token(buffer_t *buffer, char result[256], error_t *err) {
    // the 0th index of result is the type
    // sizeof(result) == 4 why?
    memset(result, '\0', 256);
    int c = peek_char(buffer);
    while(is_whitespace(c)) {
        get_next(buffer);
        c = peek_char(buffer);
    }
    if(c == '"') {
        return read_string(buffer, result, err);
    } else if(is_num(c)) {
        return read_number(buffer, result, err);
    } else if(is_letter(c)) {
        return get_next_word(buffer, result, WHITESPACE_DELIMITERS, 1, err);
    } else if (c == '#') {
        return read_hash(buffer, result, err);
    } else if(c == '.') {
        c = get_next(buffer);
        if(peek_char(buffer) == '.') {
            c = get_next(buffer);
            if(peek_char(buffer) == '.') {
                c = get_next(buffer);
                result[0] = TOKEN_TYPE_ELLIPSIS;
                result[1] = '.';
                result[2] = '.';
                result[3] = '.';
                RETURN_NO_ERR();
            } else {
                THROW_ERR("expected another . for ellipsis");
            }
        }
        result[0] = TOKEN_TYPE_SPECIAL;
        result[1] = c;
    } else if(is_special_char(c)) {
        get_next(buffer);
        result[0] = TOKEN_TYPE_SPECIAL;
        result[1] = c;
    } else if(c == EOF) {
        result[0] = TOKEN_TYPE_EOF;
        result[1] = EOF;
    } else {
        result[0] = TOKEN_TYPE_ERROR;
        result[1] = c;
        THROW_ERR("unknown character");
    }
    RETURN_NO_ERR();
}