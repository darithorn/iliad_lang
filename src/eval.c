//
// Created by darithorn on 5/3/2016.
//
#include "eval.h"
#include "util.h"
#include <stdlib.h>
#include <string.h>
#include "eval_builtins.h"

int
str_to_int(char *str) {
    int i = 0;
    for(; str[i] != '\0'; i++); // get length of str
    int result = 0;
    int place = 1;
    i--; // don't start with \0
    for(; i >= 0; i--) {
        result += ((int) str[i] - 48) * place;
        place *= 10;
    }
    return result;
}

definition_t *
new_definition(DEFINITION_TYPE type, char name[256], int index) {
    definition_t *tmp = malloc(sizeof(definition_t));
    tmp->def_type = type;
    set_str_field(tmp->name, name, 1);
    tmp->index = index;
    return tmp;
}

definition_t *
new_built_in_def(char name[256], struct value (*proc)(struct value *args, int num, error_t *err)) {
    definition_t *tmp = malloc(sizeof(definition_t));
    tmp->def_type = DEFINITION_FUNCTION;
    set_str_field(tmp->name, name, 0);
    tmp->index = 0;
    value_t val = {.data_type=DATA_FUNC, .built_in=1, .proc=proc};
    tmp->val = val;
    return tmp;
}

scope_t *
new_scope(scope_t *parent) {
    scope_t *tmp = malloc(sizeof(scope_t));
    tmp->parent = parent;
    memset(tmp->definitions, (int) NULL, 256);
    return tmp;
}

int
adv_scope_i(scope_t *s) {
    s->cur_index++;
    return s->cur_index;
}

int
bind_def(scope_t *scope, definition_t *def, error_t *err) {
    int i = 0;
    // get next available spot for definition
    for(; scope->definitions[i] != NULL; i++);
    if(i > 256) {
        THROW_ERR("too many definitions");
    }
    scope->definitions[i] = def;
    return 1;
}

int
bind_arg(scope_t *scope, char param_name[256], node_t *arg, error_t *err) {
    definition_t *def = new_definition(DEFINITION_VARIABLE, param_name, scope->cur_index);
    def->val = eval(arg, scope, err);
    def->val.built_in = 0;
    return bind_def(scope, def, err);
}

definition_t *
get_def_from_name(scope_t *scope, char name[256]) {
    int i;
    for(i = 0; scope->definitions[i] != NULL; i++) {
        int tmp = strcmp(scope->definitions[i]->name, name);
        if(tmp == 0) return scope->definitions[i];
    }
    if(scope->parent != NULL) return get_def_from_name(scope->parent, name);
    return NULL;
}

value_t
val_empty_list() {
    value_t tmp;
    tmp.built_in = 0;
    tmp.data_type = DATA_LIST;
    return tmp;
}

value_t val_list() {
    value_t tmp;
    tmp.list_val = calloc(256, sizeof(value_t));
    tmp.list_val_len = 256;
    tmp.data_type = DATA_LIST;
    tmp.built_in = 0;
    return tmp;
}

value_t
val_number(int num) {
    value_t tmp;
    tmp.built_in = 0;
    tmp.data_type = DATA_NUMBER;
    tmp.num_val = num;
    return tmp;
}

value_t
val_bool(int val) {
    value_t tmp;
    tmp.built_in = 0;
    tmp.data_type = DATA_BOOL;
    tmp.bool_val = val;
    return tmp;
}

value_t
eval_literal(node_t *node, scope_t *scope, error_t *err) {
    value_t tmp;
    switch(node->literal_type) {
        case LITERAL_STRING:
            tmp.data_type = DATA_STRING;
            set_str_field(tmp.str_val, node->literal_value[0], 0);
            break;
        case LITERAL_NUMBER:
            tmp.data_type = DATA_NUMBER;
            tmp.num_val = str_to_int(node->literal_value[0]);
            break;
        case LITERAL_WORD: {
            // TODO: quoted words should evaluate to the value of the word
            // not to their bound values if the word is defined
            if(node->node_type != NODE_QUOTE) {              
                definition_t *def = get_def_from_name(scope, node->literal_value[0]);
                if(def != NULL) {
                    return def->val;
                }
            }
            tmp.data_type = DATA_WORD;
            set_str_field(tmp.word_val, node->literal_value[0], 0);
            break;
        }
        case LITERAL_LIST:
            tmp = val_list();
            int i = 0;
            for(; i < tmp.list_val_len; i++) {
                //tmp.list_val[i] = eval
                set_str_field(tmp.list_val[i].str_val, node->literal_value[i], 0);
            }
            break;
        case LITERAL_BOOL:
            tmp.data_type = DATA_BOOL;
            // 1 because it ignores the token type #t and #f
            if(node->literal_value[0][1] == 't') tmp.bool_val = 1;
            else tmp.bool_val = 0;
            break;
        case LITERAL_CHAR:
            THROW_ERR_VAL("character literals aren't supported yet");
    }
    return tmp;
}

value_t
eval_lambda(node_t *node, scope_t *scope, error_t *err) {
    value_t tmp = {.data_type=DATA_FUNC, .func_val=node};
    return tmp;
}

value_t
eval_func_call(node_t *node, scope_t *scope, error_t *err) {
    /*if(node->node_type != NODE_LAMBDA && 
       node->node_type != NODE_LITERAL && 
       node->literal_type != LITERAL_WORD) {
        // must be either word or lambda
        printf("%i\n", node->value->node_type);
        THROW_ERR_VAL("can only call lambda or function bound to word");
    }*/
    definition_t *def = get_def_from_name(scope, node->name);
    if(def->def_type == DEFINITION_VARIABLE) {
        error_t err_tmp = {};
        err_tmp.msg = "hi";
        //*err = (error_t){};
        value_t val = val_empty_list();
        printf("hi\n");
        return val;
        //THROW_ERR_VAL("can only call word bound to function");
    }
    printf("derpi\n");
    if(node->func != NULL) {
        definition_t tmp = {DEFINITION_FUNCTION, 0, "", {DATA_NUMBER}};
        if(node->func->node_type == NODE_LAMBDA) {
            // simple workaround to avoid special cases
            // to deal with lambdas
            tmp.val.func_val = node->func;
        } else if(node->func->node_type == NODE_FUNC_CALL) {
            value_t val = eval(node->func, scope, err);
            tmp.val.func_val = val.func_val;
        }
        def = &tmp;
    }
    if(def != NULL) {
        // call the associated C function
        if(def->val.built_in != 0) {
            int i = 0;
            value_t *val_args = malloc(sizeof(value_t) * 256);
            for(; node->args[i] != NULL; i++) {
                val_args[i] = eval(node->args[i], scope, err);
            }
            value_t result = def->val.proc(val_args, i, err);
            free(val_args);
            return result;
        }
        // function is a user defined function
        scope_t *new = new_scope(scope);
        int i = 0;
        for(; node->args[i] != NULL; i++) {
            if(!bind_arg(scope, def->val.func_val->param_names[i], node->args[i], err)) return val_empty_list();
        }
        node_t **cur = &def->val.func_val->body;
        value_t tmp = val_empty_list();
        for(; *cur != NULL; cur = &((*cur)->next)) {
            // return value_t of the last evaluated node
            tmp = eval(*cur, new, err);
        }
        return tmp;
    }
    THROW_ERR_VAL("function not found");
}

value_t
eval_define_var(node_t *node, scope_t *scope, error_t *err) {
    definition_t *def = new_definition(DEFINITION_VARIABLE, node->name, scope->cur_index);
    def->val = eval(node->value, scope, err);
    bind_def(scope, def, err);
    return val_empty_list();
}

value_t
eval_define_func(node_t *node, scope_t *scope, error_t *err) {
    definition_t *def = new_definition(DEFINITION_FUNCTION, node->name, scope->cur_index);
    value_t tmp = {.data_type = DATA_FUNC, .func_val = node};
    def->val = tmp;
    bind_def(scope, def, err);
    return val_empty_list();
}

value_t
eval_if(node_t *node, scope_t *scope, error_t *err) {
    value_t val = eval(node->boolean, scope, err);
    if(val.data_type == DATA_BOOL && val.bool_val == 0) return eval(node->false, scope, err);
    return eval(node->true, scope, err);
}

value_t
eval_set(node_t *node, scope_t *scope, error_t *err) {
    definition_t *def = get_def_from_name(scope, node->name);
    if(def == NULL) THROW_ERR_VAL("cannot set an undefined variable");
    value_t new_val = eval(node->value, scope, err);
    // TODO: can check if new value type is the same as the old
    // if that's desired
    def->val = new_val;
    return val_empty_list();
}

value_t
call_func(char *name, value_t *args, int num, scope_t *global, error_t *err) {
    definition_t *def = get_def_from_name(global, name);
    if(def == NULL) {
        THROW_ERR_VAL("function is not defined");
    }
    // you can define a variable and have its value as a lambda
    // and you can 'call' that variable
    // DEFINITION_FUNCTION won't work here
    if(def->val.data_type != DATA_FUNC) {
        THROW_ERR_VAL("expected function name");
    }
    if(def->val.built_in) return def->val.proc(args, num, err);
    scope_t *scope = new_scope(global); // make a new scope for the parameters to be bound to
    int i = 0;
    for(; i < num; i++) {
        definition_t *tmp = new_definition(DEFINITION_VARIABLE, def->val.func_val->param_names[i], adv_scope_i(scope));
        if(!bind_def(scope, tmp, err)) return val_empty_list();
    }
    node_t **cur = &def->val.func_val->body;
    value_t tmp = val_empty_list();
    for(; *cur != NULL; cur = &((*cur)->next)) {
        // return value_t of the last evaluated node
        tmp = eval(*cur, scope, err);
    }
    return tmp;
}

value_t
eval(node_t *root, scope_t *scope, error_t *err) {
    scope->cur_index++;
    switch(root->node_type) {
        case NODE_LITERAL:
        case NODE_QUOTE:
            return eval_literal(root, scope, err);
        case NODE_FUNC_CALL: {
            printf("evaluating function call\n");
            value_t result = eval_func_call(root, scope, err);
        printf("%i\n", err == NULL);
        return result;
        }
        case NODE_DEFINE_VAR:
            return eval_define_var(root, scope, err);
        case NODE_DEFINE_FUNC:
            return eval_define_func(root, scope, err);
        case NODE_LAMBDA:
            return eval_lambda(root, scope, err);
        case NODE_IF:
            return eval_if(root, scope, err);
        case NODE_SET:
            return eval_set(root, scope, err);
    }
    return val_empty_list();
}

int
bind_function(char name[256], struct value (*proc)(struct value *args, int num, error_t *err), scope_t *scope,
                  error_t *err) {
    bind_def(scope, new_built_in_def(name, proc), err);
    return 1;
}

int
eval_init(scope_t *scope, error_t *err) {
    bind_function("+", &eval_num_add, scope, err);
    return 1;
}