//
// Created by darithorn on 5/10/2016.
//
#include "eval.h"
#include "eval_builtins.h"

/*
value_t THROW_ERR_VAL(error_t *err, char *msg) {
    error_t tmp_err = {.msg=msg};
    *err = tmp_err;
    value_t tmp = { .data_type=DATA_LIST };
    return tmp;
}
*/

value_t eval_eqv(value_t *args, int num) {

}

value_t eval_num_add(value_t *args, int num, error_t *err) {
    value_t result = val_number(0);
    int i = 0;
    for(; i < num; i++) {
        value_t x = args[i];
        if(x.data_type != DATA_NUMBER) THROW_ERR_VAL("ADD: all arguments must be numbers");
        result.num_val += x.num_val;
    }
    printf("%i\n", result.num_val);
    return result;
}