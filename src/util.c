//
// Created by darithorn on 5/3/2016.
//

#include <stdio.h>

int
set_str_field(char *dst, char *val, int ignore) {
    // set ignore to true if ignore val[0]
    // val[0] is the type
    int i;
    if(ignore) i = 1;
    else i = 0;
    int x = 0;
    for(; val[i] != '\0'; i++, x++) {
        dst[x] = val[i];
    }
    return 1;
}

int
expect_value(char word[256], char val[256]) {
    int i = 1; // starts at 1 to skip the first value
    for(; val[i - 1] != '\0'; i++) {
        if(word[i] != val[i - 1]) {
            return 0;
        }
    }
    return 1;
}
