//
// Created by darithorn on 5/1/2016.
//
#include "parse.h"
#include "read.h"
#include "util.h"
#include "error.h"
#include <stdlib.h>

node_t *
new_node(NODE_TYPE type) {
    node_t *tmp = malloc(sizeof(node_t));
    tmp->node_type = type;
    // they should be NULL by default
    // but that's not always the case
    // this reassures they're NULL
    tmp->next = NULL;
    tmp->body = NULL;
    tmp->func = NULL;
    tmp->boolean = NULL;
    tmp->false = NULL;
    tmp->true = NULL;
    tmp->value = NULL;
    return tmp;
}

void
free_node(node_t *node) {
    if(node->next != NULL) free_node(node->next);
    if(node->body != NULL) {
        node_t *tmp = node->body;
        for(; tmp != NULL; ) {
            node_t *next = tmp->next;
            free(tmp);
            tmp = next;
        }
    }
    if(node->value != NULL) free_node(node->value);
    free(node);
}

int
expect_type(char word[256], char type) {
    return word[0] == type;
}

int
parse_body(buffer_t *buffer, node_t **node, error_t *err) {
    char word[256];
    if(!read_token(buffer, word, err)) return 0;
    node_t **prev = &(*node)->body;
    // parse body
    for(; !expect_type(word, TOKEN_TYPE_EOF) && !expect_value(word, ")");) {
        //if(expect_type(word, TOKEN_TYPE_EOF)) THROW_ERR("unexpected end of file");
        //if(expect_value(word, ")")) break;
        if(*prev == NULL) {
            if(!parse_token(buffer, word, prev, err)) return 0;
        } else {
            if(!parse_token(buffer, word, &(*prev)->next, err)) return 0;
            prev = &(*prev)->next;
        }
        if(!read_token(buffer, word, err)) return 0;
    }
    if(expect_type(word, TOKEN_TYPE_EOF)) 
        //THROW_ERR("unexpected end of file");
    RETURN_NO_ERR();
}

int
make_literal(char word[256], node_t **node, error_t *err) {
    if(expect_type(word, TOKEN_TYPE_SPECIAL) ||
       expect_type(word, TOKEN_TYPE_EOF)) THROW_ERR("expected literal");
    *node = new_node(NODE_LITERAL);
    if(expect_type(word, TOKEN_TYPE_STRING)) (*node)->literal_type = LITERAL_STRING;
    else if(expect_type(word, TOKEN_TYPE_NUMBER)) (*node)->literal_type = LITERAL_NUMBER;
    else if(expect_type(word, TOKEN_TYPE_WORD)) (*node)->literal_type = LITERAL_WORD;
    else if(expect_type(word, TOKEN_TYPE_BOOL)) (*node)->literal_type = LITERAL_BOOL;
    else if(expect_type(word, TOKEN_TYPE_CHAR)) (*node)->literal_type = LITERAL_CHAR;
    set_str_field((*node)->literal_value[0], word, 1);
    RETURN_NO_ERR();
}

int
parse_quote(buffer_t *buffer, node_t **node, error_t *err) {
    char word[256];
    if(!read_token(buffer, word, err)) return 0;
    // TODO: support for # literals
    if(expect_value(word, "(")) {
        // list literal
        *node = new_node(NODE_QUOTE);
        (*node)->literal_type = LITERAL_LIST;
        // include the beginning (
        set_str_field((*node)->literal_value[0], word, 0);
        int i = 1; // start at 1 because we just set the first value
        int count = 1; // how many open parentheses
        for(; i < 256; i++) {
            if(!read_token(buffer, word, err)) return 0;
            set_str_field((*node)->literal_value[i], word, 0);
            if(expect_value(word, "(")) {
                count++;
            } else if(expect_value(word, ")")) {
                count--;
            }
            if(count == 0) break;
        }
        if(i == 255) THROW_ERR("currently does not support more than 256 tokens");
        if(count != 0) THROW_ERR("expected closing parenthesis");
    } else {
        // quoted words, strings, and numbers all evaluate to themselves
        int tmp = make_literal(word, node, err);
        (*node)->node_type = NODE_QUOTE;
        return tmp;
    }
    RETURN_NO_ERR();
}

int
parse_define(buffer_t *buffer, node_t **node, error_t *err) {
    *node = new_node(NODE_DEFINE_FUNC);
    char word[256];
    if(!read_token(buffer, word, err)) return 0;
    if(expect_value(word, "(")) {
        // function definition
        if(!read_token(buffer, word, err)) return 0;
        if(!expect_type(word, TOKEN_TYPE_WORD)) THROW_ERR("expected word");
        set_str_field((*node)->name, word, 0);
        // parse the parameter names
        int i = 0;
        int expect_next = 0;
        for(; i < 256; i++) {
            if(!read_token(buffer, word, err)) return 0;
            if(expect_type(word, TOKEN_TYPE_EOF)) THROW_ERR("unexpected end of file");
            if(expect_value(word, ")")) break;
            if(expect_value(word, ".")) {
                if((*node)->variadic) THROW_ERR("cannot have multiple variadic specifiers")
                (*node)->variadic = 1;
                expect_next = 1;
            } else {
                if (!expect_type(word, TOKEN_TYPE_WORD)) THROW_ERR("expected word");
                expect_next = 0;
                set_str_field((*node)->param_names[i], word, 0);
            }
        }
        if(expect_next) THROW_ERR("expected parameter name after variadic specifier");
        if(!expect_value(word, ")")) THROW_ERR("expected closing parenthesis for parameter list")
        // parse the body
        if(!parse_body(buffer, node, err)) return 0;
        if(!expect_value(word, ")")) THROW_ERR("expected closing parenthesis");
    } else if(expect_type(word, TOKEN_TYPE_WORD)) {
        // variable definition
        (*node)->node_type = NODE_DEFINE_VAR;
        set_str_field((*node)->name, word, 0);
        if(!parse_next(buffer, &((*node)->value), err)) return 0;
        if(!read_token(buffer, word, err)) return 0;
        if(!expect_value(word, ")")) THROW_ERR("expected closing parenthesis");
    } else {
        THROW_ERR("expected either function or variable definition")
    }
    RETURN_NO_ERR();
}

int
parse_if(buffer_t *buffer, node_t **node, error_t *err) {
    *node = new_node(NODE_IF);
    if(!parse_next(buffer, &(*node)->boolean, err)) return 0;
    if(!parse_next(buffer, &(*node)->true, err)) return 0;
    if(!parse_next(buffer, &(*node)->false, err)) return 0;
    char word[256];
    if(!read_token(buffer, word, err)) return 0;
    if(!expect_value(word, ")")) THROW_ERR("expected closing parenthesis");
    RETURN_NO_ERR();
}

int
parse_lambda(buffer_t *buffer, node_t **node, error_t *err) {
    char word[256];
    *node = new_node(NODE_LAMBDA);
    if(!read_token(buffer, word, err)) return 0;
    if(expect_type(word, TOKEN_TYPE_WORD)) {
        // (lambda x ...) form
        (*node)->variadic = 1;
        set_str_field((*node)->param_names[0], word, 0);
    } else {
        // (lambda (x) ...) form
        if (!expect_value(word, "(")) THROW_ERR("expected opening parenthesis for parameter list");
        int i = 0;
        // parse parameter names
        int expect_next = 0;
        for (; i < 256; i++) {
            if (!read_token(buffer, word, err)) return 0;
            if (expect_type(word, TOKEN_TYPE_EOF)) THROW_ERR("unexpected end of file");
            if (expect_value(word, ")")) break;
            if (expect_value(word, ".")) {
                if (i == 0) THROW_ERR("there must be at least one parameter before variadic specifier");
                if ((*node)->variadic) THROW_ERR("cannot have multiple variadic specifiers")
                (*node)->variadic = 1;
                expect_next = 1;
            } else {
                if (!expect_type(word, TOKEN_TYPE_WORD)) THROW_ERR("expected word");
                expect_next = 0;
                set_str_field((*node)->param_names[i], word, 0);
            }
        }
        if (expect_next) THROW_ERR("expected word after variadic specifier");
        if (!expect_value(word, ")")) THROW_ERR("expected closing parenthesis for parameter list")
    }
    if(!parse_body(buffer, node, err)) return 0;
    if(!expect_value(word, ")")) THROW_ERR("expected closing parenthesis");
    RETURN_NO_ERR();
}

int
parse_set(buffer_t *buffer, node_t **node, error_t *err) {
    *node = new_node(NODE_SET);
    char word[256];
    if(!read_token(buffer, word, err)) return 0;
    if(!expect_type(word, TOKEN_TYPE_WORD)) THROW_ERR("expected word");
    set_str_field((*node)->name, word, 1);
    if(!parse_next(buffer, &(*node)->value, err)) return 0;
    if(!read_token(buffer, word, err)) return 0;
    if(!expect_value(word, ")")) THROW_ERR("expected closing parenthesis");
    RETURN_NO_ERR();
}

int
parse_func_call(buffer_t *buffer, char name[256], node_t **node, error_t *err) {
    // name is the function name
    char word[256];
    *node = new_node(NODE_FUNC_CALL);
    if(expect_value(name, "(")) {
        // can be lambda
        // or a function that returns a function
        if (!parse_token(buffer, name, &(*node)->func, err)) return 0;
    }
    set_str_field((*node)->name, name, 1);
    int i = 0;
    node_t *tmp;
    for (; i < 256; i++) {
        if (!read_token(buffer, word, err)) return 0;
        if (expect_value(word, ")")) break;
        if (!parse_token(buffer, word, &tmp, err)) return 0;
        (*node)->args[i] = tmp;
    }
    if(!expect_value(word, ")")) THROW_ERR("expected closing parenthesis");
    RETURN_NO_ERR();
}

int
parse_expr(buffer_t *buffer, node_t **node, error_t *err) {
    // assumes the first ( was already read
    char word[256];
    if(!read_token(buffer, word, err)) return 0;
    int result;
    if(expect_value(word, "define")) {
        result = parse_define(buffer, node, err);
    } else if(expect_value(word, "if")) {
        result = parse_if(buffer, node, err);
    } else if(expect_value(word, "lambda")) {
        result = parse_lambda(buffer, node, err);
    } else if(expect_value(word, "set!")) {
        result = parse_set(buffer, node, err);
    } else {
        result = parse_func_call(buffer, word, node, err);
    }
    if(node == NULL) {
        THROW_ERR("node is null");
    }
    if(!result && node != NULL) {
        free_node(*node);
    }
    return result;
}

int
parse_token(buffer_t *buffer, char word[256], node_t **node, error_t *err) {
    if(expect_type(word, TOKEN_TYPE_SPECIAL)) {
        if(expect_value(word, "(")) {
            if(!parse_expr(buffer, node, err)) return 0;
        } else if(expect_value(word, "'")) {
            if(!parse_quote(buffer, node, err)) return 0;
        }
    } else if(expect_type(word, TOKEN_TYPE_NUMBER) ||
              expect_type(word, TOKEN_TYPE_STRING) ||
              expect_type(word, TOKEN_TYPE_WORD) ||
              expect_type(word, TOKEN_TYPE_BOOL) ||
              expect_type(word, TOKEN_TYPE_CHAR)) {
        if(!make_literal(word, node, err)) return 0;
    } else if(expect_type(word, TOKEN_TYPE_ELLIPSIS)) {

    }
    if(expect_type(word, TOKEN_TYPE_EOF)) *node = new_node(NODE_EOF);
    RETURN_NO_ERR();
}

int
parse_next(buffer_t *buffer, node_t **node, error_t *err) {
    char word[256];
    if(!read_token(buffer, word, err)) return 0;
    return parse_token(buffer, word, node, err);
}