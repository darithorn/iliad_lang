#include <stdio.h>
#include <string.h>
#include "error.h"
#include "eval.h"
#include "read.h"
#include "parse.h"

value_t urmom(value_t *args, int num, error_t *err) {
    int result = 0;
    int i = 0;
    for(;i < num; i++) {
        result += args[i].num_val;
    }
    printf("UR MOM\n");
    return val_number(result);
}

int main(int argc, char **argv) {
    if(argc < 2) {
        printf("please specify a file.\n");
        return 0;
    }
    // TODO(darithorn): fix error system
    // setting err to any value crashes the program
    error_t *err = NULL;
    buffer_t buffer = new_buffer(argv[1], err);
    
    scope_t *scope = new_scope(NULL);
    eval_init(scope, err);
    bind_function("urmom", &urmom, scope, err);
    value_t val;
    node_t *node;
    for(; ;) {
        parse_next(&buffer, &node, err);
        if(err != NULL) {
            printf("%s\n", err->msg);
            err = NULL;
        }
        if(node->node_type == NODE_EOF) break;
        val = eval(node, scope, err);
        if(err != NULL) {
            printf("error: %s\n", err->msg);
            err = NULL;
        } else {
            switch(val.data_type) {
                case DATA_NUMBER:
                    // printf("%d\n", val.num_val);
                    break;
                case DATA_STRING:
                    // printf("%s\n", val.str_val);
                    break;
                case DATA_WORD:
                    // printf("%s\n", val.word_val);
                    break;
                case DATA_LIST: {
                    // printf("list\n");
                    }
                    break;
                case DATA_BOOL:
                    if(val.bool_val) printf("#t");
                    else printf("#f");
                    break;
            }
        }
    }
    clean_buffer(&buffer);
    return 0;
}
