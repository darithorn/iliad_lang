//
// Created by darithorn on 5/1/2016.
//

#ifndef ILIAD_PARSE_H
#define ILIAD_PARSE_H

#include <stdio.h>
#include "read.h"
#include "error.h"

typedef enum {
    NODE_LITERAL,
    NODE_QUOTE,
    NODE_DEFINE_VAR,
    NODE_DEFINE_FUNC,
    NODE_IF,
    NODE_FUNC_CALL,
    NODE_LAMBDA,
    NODE_SET,
    NODE_EOF,
} NODE_TYPE;

typedef enum {
    LITERAL_NUMBER,
    LITERAL_STRING,
    LITERAL_WORD,
    LITERAL_LIST,
    LITERAL_BOOL,
    LITERAL_CHAR,
} LITERAL_TYPE;

typedef struct node_t {
    struct node_t *next;
    NODE_TYPE node_type;
    // define variable
    char name[256];
    struct node_t *value;
    // define function
    char (param_names[256])[256];
    int variadic;
    struct node_t *body;
    // value
    LITERAL_TYPE literal_type;
    char literal_value[256][256];
    // function call
    // uses name
    struct node_t *func; // this is for lambdas
    struct node_t *(args[256]);
    // if statement
    struct node_t *boolean;
    struct node_t *true;
    struct node_t *false;
} node_t;

int
parse_expr(buffer_t *buffer, node_t **node, error_t *err);

int
parse_next(buffer_t *buffer, node_t **node, error_t *err);

int
parse_token(buffer_t *buffer, char word[256], node_t **node, error_t *err);

#endif //ILIAD_PARSE_H
