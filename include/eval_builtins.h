//
// Created by darithorn on 5/10/2016.
//

#ifndef ILIAD_EVAL_BUILTINS_H
#define ILIAD_EVAL_BUILTINS_H
#include "eval.h"
#include "util.h"
#include "error.h"

#define THROW_ERR_VAL(err_msg) { error_t tmp_err = {.msg=err_msg}; *err=tmp_err;value_t tmp={.data_type=DATA_LIST};return tmp;}
// inline value_t THROW_ERR_VAL(error_t *err, char *msg);

value_t eval_eqv(value_t *args, int num);
value_t eval_eq(value_t *args, int num);
// NUMERIC
value_t eval_is_number(value_t *args, int num);
value_t eval_is_integer(value_t *args, int num);
/*
value_t
eval_is_complex(value_t *args, int num);
value_t
eval_is_real(value_t *args, int num);
value_t
eval_is_rational(value_t *args, int num);
value_t
eval_is_exact(value_t *args, int num);
value_t
eval_is_inexact(value_t *args, int num);
*/
// equal =
value_t eval_num_equal(value_t *args, int num);
// less than <
value_t eval_num_lt(value_t *args, int num);
// greater than >
value_t eval_num_gt(value_t *args, int num);
// less than equal <=
value_t eval_num_lte(value_t *args, int num);
// greater than equal >=
value_t eval_num_gte(value_t *args, int num);
value_t eval_num_add(value_t *args, int num, error_t *err);
value_t eval_num_sub(value_t *args, int num);
value_t eval_num_mul(value_t *args, int num);
value_t eval_num_div(value_t *args, int num);
// same as div
//value_t eval_num_quotient(value_t *args, int num);
value_t eval_num_remainder(value_t *args, int num);
value_t eval_num_modulo(value_t *args, int num);
// TODO: fraction support
value_t eval_num_numerator(value_t *args, int num);
value_t eval_num_denominator(value_t *args, int num);
// TODO: could this be library procs?
value_t eval_num_floor(value_t *args, int num);
value_t eval_num_ceiling(value_t *args, int num);
value_t eval_num_truncate(value_t *args, int num);
value_t eval_num_round(value_t *args, int num);
value_t eval_num_equal(value_t *args, int num);
value_t eval_num_exp(value_t *args, int num); // TODO: what is exp vs expt?
value_t eval_num_log(value_t *args, int num);
value_t eval_num_sin(value_t *args, int num);
value_t eval_num_cos(value_t *args, int num);
value_t eval_num_tan(value_t *args, int num);
value_t eval_num_asin(value_t *args, int num);
value_t eval_num_acos(value_t *args, int num);
value_t eval_num_atan(value_t *args, int num);
value_t eval_num_sqrt(value_t *args, int num);
value_t eval_num_expt(value_t *args, int num);
// END NUMERIC
// PAIR
value_t eval_is_pair(value_t *args, int num);
value_t eval_cons(value_t *args, int num);
value_t eval_car(value_t *args, int num);
value_t eval_cdr(value_t *args, int num);
// END PAIR
// SYMBOL
value_t eval_is_sym(value_t *args, int num);
value_t eval_sym_to_str(value_t *args, int num);
value_t eval_str_to_sym(value_t *args, int num);
// END SYMBOL
// CHAR
value_t eval_is_char(value_t *args, int num);
// END CHAR




#endif //ILIAD_EVAL_BUILTINS_H
