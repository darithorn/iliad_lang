//
// Created by darithorn on 5/3/2016.
//

#ifndef ILIAD_UTIL_H
#define ILIAD_UTIL_H

int
set_str_field(char *dst, char *val, int ignore);

int
expect_value(char word[256], char val[256]);
#endif //ILIAD_UTIL_H
