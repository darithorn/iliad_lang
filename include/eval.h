//
// Created by darithorn on 5/3/2016.
//

#ifndef ILIAD_EVAL_H
#define ILIAD_EVAL_H
#include "parse.h"
#include "error.h"

typedef enum {
    DATA_STRING,
    DATA_NUMBER,
    DATA_WORD,
    DATA_FUNC,
    DATA_BOOL,
    DATA_LIST,
} DATA_TYPE;

typedef struct value {
    DATA_TYPE data_type;
    int built_in;
    union {
        char str_val[256];
        int num_val;
        char word_val[256];
        struct value *list_val;
        int list_val_len;
        node_t *func_val;
        char char_val;
        int bool_val; // 0 for false 1 for true
        struct value (*proc)(struct value *args, int num, error_t *err);
    };
} value_t;

typedef enum {
    DEFINITION_VARIABLE,
    DEFINITION_FUNCTION
} DEFINITION_TYPE;

typedef struct {
    DEFINITION_TYPE def_type;
    // used to order definitions
    // to easily tell whether a definition
    // was used before actually being defined
    int index;
    char name[256];
    value_t val;
} definition_t;

typedef struct scope_t {
    struct scope_t *parent;
    int cur_index; // current index of expressions
    definition_t *(definitions[256]);
} scope_t;

value_t val_empty_list();
value_t val_number(int num);
value_t val_bool(int val);

scope_t *new_scope(scope_t *parent);
value_t eval(node_t *root, scope_t *scope, error_t *err);

int eval_init(scope_t *scope, error_t *err);
value_t call_func(char *name, value_t *args, int num, scope_t *global, error_t *err);
int bind_function(char name[256], struct value (*proc)(struct value *args, int num, error_t *err), scope_t *scope, error_t *err);

#endif //ILIAD_EVAL_H
