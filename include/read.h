#ifndef __READ_H__
#define __READ_H__
#include <stdio.h>
#include "error.h"

#define DELIMITER(str, ...) str __VA_ARGS__ 
#define WHITESPACE_DELIMITERS " \t\n\v\f\r"

static char TOKEN_TYPE_WORD = 127;
static char TOKEN_TYPE_NUMBER = 126;
static char TOKEN_TYPE_STRING = 125;
static char TOKEN_TYPE_SPECIAL = 124;
static char TOKEN_TYPE_EOF = 123;
static char TOKEN_TYPE_ERROR = 122;
static char TOKEN_TYPE_BOOL = 121;
static char TOKEN_TYPE_CHAR = 120;
static char TOKEN_TYPE_VECTOR = 119;
static char TOKEN_TYPE_ELLIPSIS = 118;

typedef struct {
    char *data;
    int size;
    int pos;
} buffer_t;

/// While not allocated directly buffer_t still needs
/// to be cleaned by freeing the data that's allocated
buffer_t
new_buffer(const char *filename, error_t *err);

/// Used to clean up the buffer data
void
clean_buffer(buffer_t *buffer);

int
get_next_word(buffer_t *buffer, char word[256], char *delimiters, int eat, error_t *err);

int
read_number(buffer_t *buffer, char number[256], error_t *err);

int
read_string(buffer_t *buffer, char str[256], error_t *err);

int
read_token(buffer_t *buffer, char result[256], error_t *err);

#endif