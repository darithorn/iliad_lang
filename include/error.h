#ifndef __ERROR_H__
#define __ERROR_H__

#define THROW_ERR(err_msg) { error_t tmp = {.msg = err_msg}; *err = tmp; return 0; }
#define RETURN_NO_ERR() { return 1; }

typedef struct {
    char *msg;
} error_t;

error_t
new_error(char *msg, ...);

void
output_error_buffer();

#endif